<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\User;
use App\Role;
use Illuminate\Support\Facades\Hash;

class TokenAuthController extends Controller
{
  public function index()
  {
      return response()->json(['auth'=>JWTAuth::parseToken()->authenticate(), 'users'=>User::all()]);
  }
  public function authenticate(Request $request)
  {
      $credentials = $request->only('email', 'password');
      try {
          if (! $token = JWTAuth::attempt($credentials)) {
              return response()->json(['error' => 'invalid_credentials'], 401);
          }
      } catch (JWTException $e) {
          return response()->json(['error' => 'could_not_create_token'], 500);
      }
      // if no errors are encountered we can return a JWT
      return response()->json(compact('token'));
  }

  public function getAuthenticatedUser()
  {
      try {

          if (! $user = JWTAuth::parseToken()->authenticate()) {
              return response()->json(['user_not_found'], 404);
          }
      } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
          return response()->json(['token_expired'], $e->getStatusCode());
      } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
          return response()->json(['token_invalid'], $e->getStatusCode());
      } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
          return response()->json(['token_absent'], $e->getStatusCode());
      }
      return response()->json(compact('user'));
  }
  public function createUser(Request $request){
      $role = User::where('email',$request->input('email'))->first();
      if($role){
        return response()->json(['success'=>'false']);
      }
      $newRole = new User();
      $newRole->email = $request->input('email');
      $newRole->password = Hash::make($request->input('password'));
      $newRole->save();
      return response()->json(['success'=>'true']);
  }
  public function createRole(Request $request){
        // Todo
        $role = Role::where('name',$request->input('name'))->first();
        if($role){
          return response()->json(['success'=>'false']);
        }
        $newRole = new Role();
        $newRole->name = $request->input('name');
        $newRole->display = $request->input('display');
        $newRole->description = $request->input('description');
        $newRole->save();
        return response()->json(['success'=>'true']);

  }

  public function createPermission(Request $request){
      // Todo
  }

  public function assignRole(Request $request){
       // Todo
  }

  public function attachPermission(Request $request){
      // Todo
  }
}
