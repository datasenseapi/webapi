<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/','HomeController@index');

Route::group(['prefix' => 'api/v1'], function()
{
  Route::post('create', 'TokenAuthController@createUser');
  Route::post('authen', 'TokenAuthController@authenticate');
  Route::get('authen/user', 'TokenAuthController@getAuthenticatedUser');
  Route::get('authen/users', 'TokenAuthController@index');
});
